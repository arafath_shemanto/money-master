// error text
let error_text = document.getElementById("error_text");
// expences function
function totalExpenses() {
  // seletion
  let food_input = document.getElementById("Food_input");
  let Rent_input = document.getElementById("Rent_input");
  let clothes_input = document.getElementById("clothes_input");
  let food_input_value = food_input.value;
  let Rent_input_value = Rent_input.value;
  let clothes_input_value = clothes_input.value;

  //   validation
  if (
    isNaN(food_input_value) ||
    isNaN(Rent_input_value) ||
    isNaN(clothes_input_value)
  ) {
    error_text.innerText = "please input number only";
  } else if (
    food_input_value.length === 0 ||
    Rent_input_value.length === 0 ||
    clothes_input_value.length === 0
  ) {
    error_text.innerText = "expences filed can't empty";
  } else if (
    food_input_value < 0 ||
    Rent_input_value < 0 ||
    clothes_input_value < 0
  ) {
    error_text.innerText = "negative number not allow";
  } else {
    error_text.innerText = "Calculate Successfully";
    let totalExpenceMoney =
      parseFloat(food_input_value) +
      parseFloat(Rent_input_value) +
      parseFloat(clothes_input_value);
    return totalExpenceMoney;
  }
}
// selection
let calculateButton = document.getElementById("Calculate_btn");
calculateButton.addEventListener("click", function () {
  // selection
  let Expenses_text = document.getElementById("Expenses_text");
  let Balance_text = document.getElementById("Balance_text");
  let incomeInputField = document.getElementById("incomeInput");
  // value
  let incomeValueText = incomeInputField.value;
  let incomeValue = parseFloat(incomeValueText);
  let monthlyTotalExpences = totalExpenses();
  console.log(monthlyTotalExpences);
  // validation checking
  if (isNaN(incomeValueText)) {
    error_text.innerText = "please input number only";
  } else if (incomeValueText < 0) {
    error_text.innerText = "negative number not allow";
  } else if (incomeValueText.length === 0) {
    error_text.innerText = "empty field not allow";
  } else if (monthlyTotalExpences > incomeValue) {
    error_text.innerText = "Expences more than income";
  }
  //  else if ()) {
  //   error_text.innerText = "Expences input number only";
  //   console.log(monthlyTotalExpences);
  // }
  else if (!isNaN(monthlyTotalExpences)) {
    //expenceBalance
    let balanceHave = incomeValue - monthlyTotalExpences;
    //currentBalance
    Balance_text.innerText = balanceHave;
    // Expense balance
    Expenses_text.innerText = monthlyTotalExpences;
  }

  //   clear value
  //   incomeInputField.value = "";
});

// ( per / 100) * 1000
// Saving handaler
document.getElementById("saving_btn").addEventListener("click", function () {
  let saving_error = document.getElementById("saving_error");
  let Balance_text = document.getElementById("Balance_text");
  let incomeInputField = document.getElementById("incomeInput");
  let incomeValueText = incomeInputField.value;
  let incomeValue = parseFloat(incomeValueText);

  //   saving_amount
  let saving_amount = document.getElementById("saving_amount");
  let remaining_balance = document.getElementById("remaining_balance");
  //   saivng
  let saving_input = document.getElementById("saving_input");
  let saving_input_text = saving_input.value;
  let saving_input_value = parseFloat(saving_input_text);
  let saving_total = (saving_input_value / 100) * incomeValue;
  let totalExpences = totalExpenses();
  let currentBalance = incomeValue - totalExpences;
  if (incomeValue < 0) {
    saving_error.innerText = "income cannot be negative ";
  }
  if (saving_input_text < 0) {
    saving_error.innerText = "percent cannot be negative";
  }
  if (isNaN(saving_input_text)) {
    saving_error.innerText = "Number Only";
  }
  if (saving_total > currentBalance) {
    saving_error.innerText = "have't  enough money";
    saving_error.style.color = "red";
  }
  if (
    !isNaN(saving_input_text) &&
    saving_total < currentBalance &&
    incomeValue > 0 &&
    saving_input_text > 0
  ) {
    saving_error.innerText = "thanks for Saving ❤ ";
    saving_error.style.color = "green";
    saving_amount.innerText = saving_total;
    remaining_balance.innerText = currentBalance - saving_total;
  }
});
